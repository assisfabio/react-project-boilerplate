import React, { Component } from 'react';
import { Header, Panel, Resume } from './components/exports';
import './App.scss';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Panel />
        <Resume />
      </div>
    );
  }
}

export default App;
