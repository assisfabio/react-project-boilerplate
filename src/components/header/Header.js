import React, { Component } from 'react';
import logo from './logo.svg';
import './Header.scss';

class Header extends Component {
  render() {
    return (
      <header className="header">
        <h1 className="logo">
          <a><img src={logo} alt="logo" /></a>
        </h1>
      </header>
    );
  }
}

export default Header;
