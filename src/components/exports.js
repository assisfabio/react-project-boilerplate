import Header from './header/Header';
import Resume from './resume/Resume';
import Panel from './panel/Panel';

export {
    Header,
    Resume,
    Panel
};